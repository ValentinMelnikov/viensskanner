﻿using Ninject.Modules;
using ViensScanner.AppService;
using ViensScanner.Contract;

namespace ViensScanner.UI
{
    public class ViensScannerResolver : NinjectModule
    {
        public override void Load()
        {
            Bind<IImageService>().To<ImageService>();
        }
    }
}
