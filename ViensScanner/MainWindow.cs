﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Ninject;
using ViensScanner.Contract;

namespace ViensScanner.UI
{
    public partial class MainWindow : Form
    {
        private bool _isLoad;

        private readonly IImageService _imageService;

        public MainWindow()
        {
            var kernel = new StandardKernel(new ViensScannerResolver());
            _imageService = kernel.Get<IImageService>();
            InitializeComponent();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = @"Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png, *.bmp) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png; *.bmp"
            };
            if (openFileDialog.ShowDialog() != DialogResult.OK) return;
            using (var fs = new FileStream(openFileDialog.FileName, FileMode.Open))
            {
                beforePictureBox.Image = Image.FromStream(fs);
            }           
            _isLoad = true;
        }

        private void grayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_isLoad)
            {
                afterPictureBox.Image = _imageService.GrayShadin(new Bitmap(beforePictureBox.Image));
            }
            else
            {
                MessageBox.Show(@"Image not load", @"Error");
            }
        }

        private void contrastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_isLoad)
            {
                beforePictureBox.Image = afterPictureBox.Image;
                afterPictureBox.Image = _imageService.Contrastin(new Bitmap(beforePictureBox.Image));
            }
            else
            {
                MessageBox.Show(@"Image not load", @"Error");
            }
        }

        private void equalizationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_isLoad)
            {
                beforePictureBox.Image = afterPictureBox.Image;
                afterPictureBox.Image = _imageService.Equalization(new Bitmap(beforePictureBox.Image));
            }
            else
            {
                MessageBox.Show(@"Image not load", @"Error");
            }
        }

        private void filterinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_isLoad)
            {
                beforePictureBox.Image = afterPictureBox.Image;
                afterPictureBox.Image = _imageService.Filterin(new Bitmap(beforePictureBox.Image));
            }
            else
            {
                MessageBox.Show(@"Image not load", @"Error");
            }
        }

        private void binarizationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_isLoad)
            {
                beforePictureBox.Image = afterPictureBox.Image;
                afterPictureBox.Image = _imageService.Binarization(new Bitmap(beforePictureBox.Image));
            }
            else
            {
                MessageBox.Show(@"Image not load", @"Error");
            }
        }

        private void localBinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_isLoad)
            {
                afterPictureBox.Image = _imageService.LocalBinarization(new Bitmap(beforePictureBox.Image), new Bitmap(afterPictureBox.Image));
            }
            else
            {
                MessageBox.Show(@"Image not load", @"Error");
            }
        }

        private void thinningToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_isLoad)
            {
                afterPictureBox.Image = _imageService.Thinning(new Bitmap(afterPictureBox.Image));
            }
            else
            {
                MessageBox.Show(@"Image not load", @"Error");
            }
        }

        private void findSingularPointsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_isLoad)
            {
                afterPictureBox.Image = _imageService.FindSingularPoints(new Bitmap(afterPictureBox.Image));
            }
            else
            {
                MessageBox.Show(@"Image not load", @"Error");
            }
        }

        private void edgeDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_isLoad)
            {
                afterPictureBox.Image = _imageService.EdgeDetection(new Bitmap(afterPictureBox.Image));
            }
            else
            {
                MessageBox.Show(@"Image not load", @"Error");
            }
        }

        private void detectToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
