﻿using System.Drawing;
using Ninject;
using ViensScanner.Contract;
using ViensScanner.Domain.Contracts;

namespace ViensScanner.AppService
{
    public class ImageService : IImageService
    {
        private readonly IGrayShadinAlgorithm _grayShadinAlgorithm;
        private readonly IContrastinAlgorithm _contrastinAlgorithm;
        private readonly IFilterinAlgorithm _filterinAlgorithm;
        private readonly IEqualizationAlgorithm _equalizationAlgorithm;
        private readonly IBinarizationAlgorithm _binarizationAlgorithm;
        private readonly IThinningAlgorithm _thinningAlgorithm;
        private readonly IFindSingularPointsAlgotithm _findSingularPointsAlgorithm;
        private readonly IEdgeDetectionAlgorithm _edgeDetectionAlgorithm;

        public ImageService()
        {
            var kernel = new StandardKernel(new ImageServiceResolver());
            _grayShadinAlgorithm = kernel.Get<IGrayShadinAlgorithm>();
            _contrastinAlgorithm = kernel.Get<IContrastinAlgorithm>();
            _filterinAlgorithm = kernel.Get<IFilterinAlgorithm>();
            _equalizationAlgorithm = kernel.Get<IEqualizationAlgorithm>();
            _binarizationAlgorithm = kernel.Get<IBinarizationAlgorithm>();
            _thinningAlgorithm = kernel.Get<IThinningAlgorithm>();
            _findSingularPointsAlgorithm = kernel.Get<IFindSingularPointsAlgotithm>();
            _edgeDetectionAlgorithm = kernel.Get<IEdgeDetectionAlgorithm>();
        }

        public Bitmap GrayShadin(Bitmap image)
        {
            return _grayShadinAlgorithm.GrayShadin(image);
        }

        public Bitmap Contrastin(Bitmap image)
        {
            return _contrastinAlgorithm.Contrastin(image);
        }

        public Bitmap Filterin(Bitmap image)
        {
            return _filterinAlgorithm.Filterin(image);
        }

        public Bitmap Equalization(Bitmap image)
        {
            return _equalizationAlgorithm.Equalization(image);
        }

        public Bitmap Binarization(Bitmap image)
        {
            return _binarizationAlgorithm.Binarization(image);
        }

        public Bitmap LocalBinarization(Bitmap image, Bitmap mask)
        {
            return _binarizationAlgorithm.LocalBinarization(image, mask);
        }

        public Bitmap Thinning(Bitmap image)
        {
            return _thinningAlgorithm.Thinning(image);
        }

        public Bitmap FindSingularPoints(Bitmap image)
        {
            return _findSingularPointsAlgorithm.Find(image);
        }

        public Bitmap EdgeDetection(Bitmap image)
        {
            return _edgeDetectionAlgorithm.Detect(image);
        }
    }
}