﻿using Ninject.Modules;
using ViensScanner.Domain.Algorithms;
using ViensScanner.Domain.Algorithms.Binarization;
using ViensScanner.Domain.Contracts;

namespace ViensScanner.AppService
{
    internal class ImageServiceResolver : NinjectModule
    {
        public override void Load()
        {
            Bind<IGrayShadinAlgorithm>().To<AverageGreyShadinAlgorithm>();
            Bind<IFilterinAlgorithm>().To<MedianFilterinAlgorithm>();
            Bind<IContrastinAlgorithm>().To<LinearContrastinAlgorithm>();
            Bind<IEqualizationAlgorithm>().To<AverageEqualizationAlgorithm>();
            Bind<IBinarizationAlgorithm>().To<NiblackBinarizationAlgorithm>();
            Bind<IThinningAlgorithm>().To<ZhangSuenThinningAlgorithm>();
            Bind<IFindSingularPointsAlgotithm>().To<FindSingularPointsSimpleAlgorithm>();
            Bind<IEdgeDetectionAlgorithm>().To<CannyEdgeDetectionAlgorithm>();
        }
    }
}
