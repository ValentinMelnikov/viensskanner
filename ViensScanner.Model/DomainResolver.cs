﻿using Data.ContractRepository;
using FileRepository;
using Ninject.Modules;

namespace ViensScanner.Domain
{
    public class DomainResolver : NinjectModule
    {
        public override void Load()
        {
            Bind<ISingularPointRepository>().To<SingularPointRepository>();
        }
    }
}
