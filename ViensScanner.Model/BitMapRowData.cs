﻿namespace ViensScanner.Domain
{
    public class BitmapRawData
    {
        public BitmapRawData(int[] rgbArray, int[] alphaArray, int width, int height)
        {
            RgbArray = rgbArray;
            AlphaArray = alphaArray;
            Width = width;
            Height = height;
        }

        public int Width { get; set; }

        public int Height { get; set; }

        public int[] RgbArray { get; set; }

        public int[] AlphaArray { get; set; }
    }
}
