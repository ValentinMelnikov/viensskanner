﻿using System.Drawing;

namespace ViensScanner.Domain.Contracts
{
    public interface IGrayShadinAlgorithm
    {
        Bitmap GrayShadin(Bitmap image);
    }
}
