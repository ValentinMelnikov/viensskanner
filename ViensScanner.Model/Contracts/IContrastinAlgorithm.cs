﻿using System.Drawing;

namespace ViensScanner.Domain.Contracts
{
    public interface IContrastinAlgorithm
    {
        Bitmap Contrastin(Bitmap image);
    }
}
