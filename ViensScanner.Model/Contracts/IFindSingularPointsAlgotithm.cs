﻿using System.Drawing;

namespace ViensScanner.Domain.Contracts
{
    public interface IFindSingularPointsAlgotithm
    {
        Bitmap Find(Bitmap image);
    }
}