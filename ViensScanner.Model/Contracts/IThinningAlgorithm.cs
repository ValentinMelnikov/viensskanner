﻿using System.Drawing;

namespace ViensScanner.Domain.Contracts
{
    public interface IThinningAlgorithm
    {
        Bitmap Thinning(Bitmap image);
    }
}
