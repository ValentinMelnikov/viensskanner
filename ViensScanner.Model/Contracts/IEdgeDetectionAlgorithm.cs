﻿using System.Drawing;

namespace ViensScanner.Domain.Contracts
{
    public interface IEdgeDetectionAlgorithm
    {
        Bitmap Detect(Bitmap image);
    }
}
