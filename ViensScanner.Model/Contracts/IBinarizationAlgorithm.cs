﻿using System.Drawing;

namespace ViensScanner.Domain.Contracts
{
    public interface IBinarizationAlgorithm
    {
        Bitmap Binarization(Bitmap image);

        Bitmap LocalBinarization(Bitmap image, Bitmap mask);
    }
}
