﻿using System.Drawing;

namespace ViensScanner.Domain.Contracts
{
    public interface IEqualizationAlgorithm
    {
        Bitmap Equalization(Bitmap image);
    }
}
