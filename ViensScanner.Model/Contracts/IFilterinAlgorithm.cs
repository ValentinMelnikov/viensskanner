﻿using System.Drawing;

namespace ViensScanner.Domain.Contracts
{
    public interface IFilterinAlgorithm
    {
        Bitmap Filterin(Bitmap image);
    }
}
