﻿using System;
using System.Collections.Generic;
using System.Drawing;
using ViensScanner.Domain.Contracts;

namespace ViensScanner.Domain.Algorithms
{
    public class MedianFilterinAlgorithm : IFilterinAlgorithm
    {
        public Bitmap Filterin(Bitmap image)
        {
            var rgb = new int[3];
            var img = new Bitmap(image.Width, image.Height);
            for (var i = 1; i < img.Width - 1; i++)
            {
                for (var j = 1; j < img.Height - 1; j++)
                {
                    rgb[0] = CalculatePixel(image, Rgb.R, i, j);
                    rgb[1] = CalculatePixel(image, Rgb.G, i, j);
                    rgb[2] = CalculatePixel(image, Rgb.B, i, j);
                    img.SetPixel(i, j, Color.FromArgb(rgb[0], rgb[1], rgb[2]));
                }
            }
            return img;
        }

        private int CalculatePixel(Bitmap img, Rgb rgbEnum, int x, int y)
        {
            var windowCoeff = new List<int>();
            switch (rgbEnum)
            {
                case Rgb.R:
                    windowCoeff.Add(img.GetPixel(x, y).R);
                    windowCoeff.Add(img.GetPixel(x, y).R);
                    windowCoeff.Add(img.GetPixel(x, y).R);
                    windowCoeff.Add(img.GetPixel(x - 1, y).R);
                    windowCoeff.Add(img.GetPixel(x - 1, y).R);
                    windowCoeff.Add(img.GetPixel(x + 1, y).R);
                    windowCoeff.Add(img.GetPixel(x + 1, y).R);
                    windowCoeff.Add(img.GetPixel(x, y - 1).R);
                    windowCoeff.Add(img.GetPixel(x, y + 1).R);
                    break;
                case Rgb.G:
                    windowCoeff.Add(img.GetPixel(x, y).G);
                    windowCoeff.Add(img.GetPixel(x, y).G);
                    windowCoeff.Add(img.GetPixel(x, y).G);
                    windowCoeff.Add(img.GetPixel(x - 1, y).G);
                    windowCoeff.Add(img.GetPixel(x - 1, y).G);
                    windowCoeff.Add(img.GetPixel(x + 1, y).G);
                    windowCoeff.Add(img.GetPixel(x + 1, y).G);
                    windowCoeff.Add(img.GetPixel(x, y - 1).G);
                    windowCoeff.Add(img.GetPixel(x, y + 1).G);
                    break;
                case Rgb.B:
                    windowCoeff.Add(img.GetPixel(x, y).B);
                    windowCoeff.Add(img.GetPixel(x, y).B);
                    windowCoeff.Add(img.GetPixel(x, y).B);
                    windowCoeff.Add(img.GetPixel(x - 1, y).B);
                    windowCoeff.Add(img.GetPixel(x - 1, y).B);
                    windowCoeff.Add(img.GetPixel(x + 1, y).B);
                    windowCoeff.Add(img.GetPixel(x + 1, y).B);
                    windowCoeff.Add(img.GetPixel(x, y - 1).B);
                    windowCoeff.Add(img.GetPixel(x, y + 1).B);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(rgbEnum), rgbEnum, null);
            }
            windowCoeff.Sort();
            return windowCoeff[windowCoeff.Count/2];
        }
    }
}
