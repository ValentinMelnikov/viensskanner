﻿using System;
using System.Drawing;
using ViensScanner.Domain.Contracts;

namespace ViensScanner.Domain.Algorithms
{
    public class LinearContrastinAlgorithm : IContrastinAlgorithm
    {
        public Bitmap Contrastin(Bitmap image)
        {
            var convertedImage = BitmapImageConverter.ConvertToRawDataFromImage(image);
            var rgbArray = new int[convertedImage.RgbArray.Length];
            Array.Copy(convertedImage.RgbArray, rgbArray, convertedImage.RgbArray.Length);
            var minColor = byte.MaxValue;
            var maxColor = byte.MinValue;
            foreach (var t in rgbArray)
            {
                if (minColor > t)
                    minColor = (byte)t;
                if (maxColor < t)
                    maxColor = (byte)t;
            }
            var value = (double)255 / (maxColor - minColor);
            for (var i = 0; i < rgbArray.Length; i++)
            {
                rgbArray[i] = (byte)(value * (rgbArray[i] - minColor));
            }
            var newBitmapRd = new BitmapRawData(rgbArray, convertedImage.AlphaArray, convertedImage.Width,
                convertedImage.Height);
            return BitmapImageConverter.ConvertToImageFromRawData(newBitmapRd);
        }
    }
}
