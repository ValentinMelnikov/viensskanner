﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using ViensScanner.Domain.Algorithms.Binarization;
using ViensScanner.Domain.Contracts;

namespace ViensScanner.Domain.Algorithms
{
    public class CannyEdgeDetectionAlgorithm : IEdgeDetectionAlgorithm
    {
        private const int GaussKernelSize = 5;
        private const double GaussKernelDeviation = 1.4;
        private static Bitmap _afterGaussImg;
        private static Bitmap _afterCannyImg;
        private static Bitmap _afterSupressionImg;
        private static Bitmap _afterBinarizationImg;
        private static Bitmap _afterFillVeinImg;
        private static double[,] _gaussKernel;
        private static double[,] _gradientX;
        private static double[,] _gradientY;
        private static double[,] _gradientDirections;
        private const int MaxIteration = 20;
        private const int GradTreshold = 0;

        private static readonly double[,] CannyKernelX = {
            {-1, 0, 1},
            {-2, 0, 2},
            {-1, 0, 1}
        };

        private static readonly double[,] CannyKernelY = {
            {1, 2, 1},
            {0, 0, 0},
            {-1, -2, -1}
        };

        public Bitmap Detect(Bitmap image)
        {
            CalculateGaussKernel(GaussKernelSize, GaussKernelDeviation);
            _afterGaussImg = ApplyFilter(image, _gaussKernel);
            _afterCannyImg = GenerateGradients(_afterGaussImg);
            _afterSupressionImg = NonMaximumSuppression(_afterCannyImg);
            var bin = new ManualBinarizationAlgorithm();
            _afterBinarizationImg = bin.Binarization(_afterSupressionImg);
            _afterFillVeinImg = _afterBinarizationImg;
            var i = 0;
            while (i++ != MaxIteration)
            {
                _afterFillVeinImg = FillVein(_afterFillVeinImg);
            }
            return _afterBinarizationImg;
        }

        private static void CalculateGaussKernel(int length, double weight)
        {
            _gaussKernel = new double[length, length];
            double sumTotal = 0;
            var kernelRadius = length / 2;
            var calculatedEuler = 1.0 / (2.0 * Math.PI * Math.Pow(weight, 2));
            for (var filterY = -kernelRadius; filterY <= kernelRadius; filterY++)
            for (var filterX = -kernelRadius; filterX <= kernelRadius; filterX++)
            {
                var distance = (filterX * filterX + filterY * filterY) / (2 * (weight * weight));
                _gaussKernel[filterY + kernelRadius, filterX + kernelRadius] = calculatedEuler * Math.Exp(-distance);
                sumTotal += _gaussKernel[filterY + kernelRadius, filterX + kernelRadius];
            }
            for (var y = 0; y < length; y++)
            for (var x = 0; x < length; x++)
                _gaussKernel[y, x] = _gaussKernel[y, x] * (1.0 / sumTotal);
        }

        private static Bitmap ApplyFilter(Bitmap sourceBitmap, double[,] kernel)
        {
            var sourceData = sourceBitmap.LockBits(new Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height),
                ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            var pixelBuffer = new byte[sourceData.Stride * sourceData.Height];
            var resultBuffer = new byte[sourceData.Stride * sourceData.Height];
            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
            sourceBitmap.UnlockBits(sourceData);
            var filterWidth = kernel.GetLength(1);
            var filterOffset = (filterWidth - 1) / 2;
            for (var offsetY = filterOffset; offsetY < sourceBitmap.Height - filterOffset; offsetY++)
            for (var offsetX = filterOffset; offsetX < sourceBitmap.Width - filterOffset; offsetX++)
            {
                double blue = 0;
                double green = 0;
                double red = 0;
                var byteOffset = offsetY * sourceData.Stride + offsetX * 4;
                for (var filterY = -filterOffset; filterY <= filterOffset; filterY++)
                for (var filterX = -filterOffset; filterX <= filterOffset; filterX++)
                {
                    var calcOffset = byteOffset + filterX * 4 + filterY * sourceData.Stride;
                    blue += pixelBuffer[calcOffset] * kernel[filterY + filterOffset, filterX + filterOffset];
                    green += pixelBuffer[calcOffset + 1] * kernel[filterY + filterOffset, filterX + filterOffset];
                    red += pixelBuffer[calcOffset + 2] * kernel[filterY + filterOffset, filterX + filterOffset];
                }
                blue = blue > 255 ? 255 : blue;
                green = green > 255 ? 255 : green;
                red = red > 255 ? 255 : red;
                resultBuffer[byteOffset] = (byte) blue;
                resultBuffer[byteOffset + 1] = (byte) green;
                resultBuffer[byteOffset + 2] = (byte) red;
                resultBuffer[byteOffset + 3] = 255;
            }
            var resultBitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height);
            var resultData = resultBitmap.LockBits(new Rectangle(0, 0, resultBitmap.Width, resultBitmap.Height),
                ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
            Marshal.Copy(resultBuffer, 0, resultData.Scan0, resultBuffer.Length);
            resultBitmap.UnlockBits(resultData);

            return resultBitmap;
        }

        private static Bitmap GenerateGradients(Bitmap sourceBitmap)
        {
            var sourceData = sourceBitmap.LockBits(new Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height),
                ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            var pixelBuffer = new byte[sourceData.Stride * sourceData.Height];
            var resultBuffer = new byte[sourceData.Stride * sourceData.Height];
            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
            sourceBitmap.UnlockBits(sourceData);
            _gradientX = new double[sourceBitmap.Height, sourceBitmap.Width];
            _gradientY = new double[sourceBitmap.Height, sourceBitmap.Width];
            _gradientDirections = new double[sourceBitmap.Height, sourceBitmap.Width];
            var filterWidth = CannyKernelX.GetLength(1);
            var filterOffset = (filterWidth - 1) / 2;
            for (var offsetX = filterOffset; offsetX < sourceBitmap.Height - filterOffset; offsetX++)
                for (var offsetY = filterOffset; offsetY < sourceBitmap.Width - filterOffset; offsetY++)
                {
                    double gradX = 0;
                    double gradY = 0;
                    var byteOffset = offsetX * sourceData.Stride + offsetY * 4;
                    for (var filterY = -filterOffset; filterY <= filterOffset; filterY++)
                        for (var filterX = -filterOffset; filterX <= filterOffset; filterX++)
                        {
                            var calcOffset = byteOffset + filterY * 4 + filterX * sourceData.Stride;
                            gradX += pixelBuffer[calcOffset] * CannyKernelX[filterX + filterOffset, filterY + filterOffset];
                            gradY += pixelBuffer[calcOffset] * CannyKernelY[filterX + filterOffset, filterY + filterOffset];
                        }
                    _gradientX[offsetX, offsetY] = gradX;
                    _gradientY[offsetX, offsetY] = gradY;
                    _gradientDirections[offsetX, offsetY] = Math.Atan(gradY / (Math.Abs(gradX) < 0.1 ? 0.01 : gradX));
                    var pixelValue = Math.Sqrt(gradX * gradX + gradY * gradY);
                    pixelValue = pixelValue > 255 ? 0 : pixelValue;
                    resultBuffer[byteOffset] = (byte) pixelValue;
                    resultBuffer[byteOffset + 1] = (byte) pixelValue;
                    resultBuffer[byteOffset + 2] = (byte) pixelValue;
                    resultBuffer[byteOffset + 3] = 255;
                }
            var resultBitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height);
            var resultData = resultBitmap.LockBits(new Rectangle(0, 0, resultBitmap.Width, resultBitmap.Height),
                ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
            Marshal.Copy(resultBuffer, 0, resultData.Scan0, resultBuffer.Length);
            resultBitmap.UnlockBits(resultData);
            return resultBitmap;
        }

        private static Bitmap NonMaximumSuppression(Bitmap image)
        {
            var returnMap = new Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb);
            var bitmapData1 = image.LockBits(new Rectangle(0, 0, image.Width, image.Height),
                ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            var bitmapData2 = returnMap.LockBits(new Rectangle(0, 0, returnMap.Width, returnMap.Height),
                ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

            unsafe
            {
                var imagePointer1 = (byte*) bitmapData1.Scan0 + bitmapData1.Stride + 4;
                var imagePointer2 = (byte*) bitmapData2.Scan0 + bitmapData2.Stride + 4;
                for (var i = 1; i < bitmapData1.Height - 1; i++)
                {
                    for (var j = 1; j < bitmapData1.Width - 1; j++)
                    {
                        var grad = _gradientDirections[i, j];
                        if ((grad >= Math.PI * 3 / 8 || grad <= -Math.PI * 3 / 8) &&
                            (imagePointer1[-bitmapData1.Stride] > imagePointer1[0] ||
                             imagePointer1[bitmapData1.Stride] > imagePointer1[0]) ||
                            grad > Math.PI * 1 / 8 && grad < Math.PI * 3 / 8 &&
                            (imagePointer1[-bitmapData1.Stride + 4] > imagePointer1[0] ||
                             imagePointer1[bitmapData1.Stride - 4] > imagePointer1[0]) ||
                            grad <= Math.PI * 1 / 8 && grad >= -Math.PI * 1 / 8 &&
                            (imagePointer1[-4] > imagePointer1[0] || imagePointer1[4] > imagePointer1[0]) ||
                            grad < -Math.PI * 1 / 8 && grad > -Math.PI * 3 / 8 &&
                            (imagePointer1[-bitmapData1.Stride - 4] > imagePointer1[0] ||
                             imagePointer1[bitmapData1.Stride + 4] > imagePointer1[0]))
                        {
                            imagePointer2[0] = 0;
                            imagePointer2[1] = 0;
                            imagePointer2[2] = 0;
                        }
                        else
                        {
                            imagePointer2[0] = imagePointer1[0];
                            imagePointer2[1] = imagePointer1[1];
                            imagePointer2[2] = imagePointer1[2];
                        }
                        imagePointer2[3] = 255;
                        imagePointer1 += 4;
                        imagePointer2 += 4;
                    }
                    imagePointer1 += 8;
                    imagePointer2 += 8;
                }
            }
            returnMap.UnlockBits(bitmapData2);
            image.UnlockBits(bitmapData1);
            return returnMap;
        }

        private static Bitmap FillVein(Bitmap image)
        {
            var bitmapRd = BitmapImageConverter.ConvertToRawDataFromImage(image);
            var rgbArray = new int[bitmapRd.Width * bitmapRd.Height];
            var fillRgbArray = new int[bitmapRd.Width * bitmapRd.Height];
            for (var i = 0; i < bitmapRd.RgbArray.Length; i++)
            {
                if (i % 3 == 0)
                {
                    rgbArray[i / 3] = bitmapRd.RgbArray[i];
                }
            }
            Array.Copy(rgbArray, fillRgbArray, rgbArray.Length);
            for (var i = 1; i < bitmapRd.Height - 1; i++)
            {
                for (var j = 1; j < bitmapRd.Width - 1; j++)
                {
                    var grad = _gradientDirections[i, j];
                    var gradValue = (Math.Abs(_gradientX[i, j]) + Math.Abs(_gradientY[i, j]))/2;
                    if (_afterFillVeinImg.GetPixel(j, i).R == 255 || (gradValue < GradTreshold)) continue;
                    if (grad <= Math.PI*1/8 && grad >= -Math.PI*1/8)
                    {
                        //fillRgbArray[i * bitmapRd.Width + j + 1] = 0;
                        fillRgbArray[i * bitmapRd.Width + j - 1] = 0;
                    }
                    if (grad <= Math.PI*3/8 && grad >= Math.PI*1/8)
                    {
                        //fillRgbArray[(i - 1) * bitmapRd.Width + j + 1] = 0;
                        fillRgbArray[(i + 1) * bitmapRd.Width + j - 1] = 0;
                    }
                    if (grad <= Math.PI*5/8 && grad >= Math.PI*3/8)
                    {
                        fillRgbArray[(i + 1) * bitmapRd.Width + j] = 0;
                    }
                    if (grad <= -Math.PI*1/8 && grad >= -Math.PI*3/8)
                    {
                        fillRgbArray[(i - 1) * bitmapRd.Width + j - 1] = 0;
                        //fillRgbArray[(i + 1) * bitmapRd.Width + j + 1] = 0;
                    }
                    if (grad <= -Math.PI*5/8 && grad >= -Math.PI*3/8)
                    {
                        fillRgbArray[(i - 1) * bitmapRd.Width + j] = 0;
                    }
                }
            }
            var l = 0;
            for (var i = 0; i < rgbArray.Length; i++)
            {
                bitmapRd.RgbArray[l++] = fillRgbArray[i];
                bitmapRd.RgbArray[l++] = fillRgbArray[i];
                bitmapRd.RgbArray[l++] = fillRgbArray[i];
            }
            var newBitmapRd = new BitmapRawData(bitmapRd.RgbArray, bitmapRd.AlphaArray, bitmapRd.Width, bitmapRd.Height);
            return BitmapImageConverter.ConvertToImageFromRawData(newBitmapRd);
        }
    }
}