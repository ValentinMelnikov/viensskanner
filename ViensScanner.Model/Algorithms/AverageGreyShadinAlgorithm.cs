﻿using System.Drawing;
using ViensScanner.Domain.Contracts;

namespace ViensScanner.Domain.Algorithms
{
    public class AverageGreyShadinAlgorithm : IGrayShadinAlgorithm
    {
        public Bitmap GrayShadin(Bitmap image)
        {
            var convertedImage = BitmapImageConverter.ConvertToRawDataFromImage(image);
            var rgbArray = convertedImage.RgbArray;
            var result = new int[rgbArray.Length];
            for (var i = 0; i < result.Length / 3; i++)
            {
                result[3 * i] = (rgbArray[3 * i] + rgbArray[3 * i + 1] + rgbArray[3 * i + 2]) / 3;
                result[3 * i + 1] = (rgbArray[3 * i] + rgbArray[3 * i + 1] + rgbArray[3 * i + 2]) / 3;
                result[3 * i + 2] = (rgbArray[3 * i] + rgbArray[3 * i + 1] + rgbArray[3 * i + 2]) / 3;
            }
            convertedImage.RgbArray = result;
            return BitmapImageConverter.ConvertToImageFromRawData(convertedImage);
        }
    }
}
