﻿using System.Collections.Generic;
using System.Drawing;
using Data;
using Data.ContractRepository;
using Ninject;
using ViensScanner.Domain.Contracts;

namespace ViensScanner.Domain.Algorithms
{
    public class FindSingularPointsSimpleAlgorithm : IFindSingularPointsAlgotithm
    {
        private readonly ISingularPointRepository _singularPointRepository;

        public FindSingularPointsSimpleAlgorithm()
        {
            var kernel = new StandardKernel(new DomainResolver());
            _singularPointRepository = kernel.Get<ISingularPointRepository>();
        }

        public Bitmap Find(Bitmap image)
        {
            var bitmapRd = BitmapImageConverter.ConvertToRawDataFromImage(image);
            var rgbArray = new int[bitmapRd.Width * bitmapRd.Height];
            for (var i = 0; i < bitmapRd.RgbArray.Length; i++)
            {
                if (i % 3 == 0)
                {
                    rgbArray[i / 3] = bitmapRd.RgbArray[i];
                }
            }
            var bmp = new Bitmap(bitmapRd.Width, bitmapRd.Height);
            using (var graph = Graphics.FromImage(bmp))
            {
                graph.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));
            }
            for (var i = 1; i < bitmapRd.Height - 1; i++)
            {
                for (var j = 1; j < bitmapRd.Width - 1; j++)
                {
                    if (rgbArray[i*bitmapRd.Width + j] != 0) continue;
                    var sum = GetSum(rgbArray, i, j, bitmapRd.Width);
                    if (sum == (int) SingularPoints.End)
                    {
                        bmp.SetPixel(j,i,Color.Blue);
                        _singularPointRepository.Save(new SingularPoint()
                        {
                            X = i,
                            Y = j,
                            Type = SingularPoints.End
                        });
                    }
                    if (sum == (int)SingularPoints.Bifurcation)
                    {
                        bmp.SetPixel(j, i, Color.Brown);
                        _singularPointRepository.Save(new SingularPoint()
                        {
                            X = i,
                            Y = j,
                            Type = SingularPoints.Bifurcation
                        });
                    }
                    if (sum == (int)SingularPoints.Crossing)
                    {
                        bmp.SetPixel(j, i, Color.Chartreuse);
                        _singularPointRepository.Save(new SingularPoint()
                        {
                            X = i,
                            Y = j,
                            Type = SingularPoints.Crossing
                        });
                    }
                }
            }
            return bmp;
        }

        private static int GetSum(IReadOnlyList<int> rgbArray, int i, int j, int width)
        {
            var sum = 0;
            
            if (rgbArray[(i - 1) * width + j] == 0)
                sum++;
            if (rgbArray[(i - 1) * width + (j + 1)] == 0)
                sum++;
            if (rgbArray[i * width + (j + 1)] == 0)
                sum++;
            if (rgbArray[(i + 1) * width + (j + 1)] == 0)
                sum++;
            if (rgbArray[(i + 1) * width + j] == 0)
                sum++;
            if (rgbArray[(i + 1) * width + (j - 1)] == 0)
                sum++;
            if (rgbArray[i * width + (j - 1)] == 0)
                sum++;
            if (rgbArray[(i - 1) * width + (j - 1)] == 0)
                sum++;
            return sum;
        }
    }
}
