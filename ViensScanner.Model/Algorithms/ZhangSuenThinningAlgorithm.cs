﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ViensScanner.Domain.Contracts;

namespace ViensScanner.Domain.Algorithms
{
    public class ZhangSuenThinningAlgorithm : IThinningAlgorithm
    {
        public Bitmap Thinning(Bitmap image)
        {
            var bitmapRd = BitmapImageConverter.ConvertToRawDataFromImage(image);
            var rgbArray = new int[bitmapRd.Width * bitmapRd.Height];
            var thinRgbArray = new int[bitmapRd.Width * bitmapRd.Height];
            for (var i = 0; i < bitmapRd.RgbArray.Length; i++)
            {
                if (i % 3 == 0)
                {
                    rgbArray[i / 3] = bitmapRd.RgbArray[i];
                }
            }
            var isChange = true;
            while (isChange)
            {
                isChange = false;
                for (var i = 1; i < bitmapRd.Height - 1; i++)
                {
                    for (var j = 1; j < bitmapRd.Width - 1; j++)
                    {
                        if (rgbArray[i * bitmapRd.Width + j] == 255)
                        {
                            thinRgbArray[i * bitmapRd.Width + j] = 255;
                            continue;
                        }
                        var window = GetWindow(rgbArray, i, j, bitmapRd.Width);
                        var sum = window.Sum() - window[0];
                        var jumpCount = 0;
                        for (var k = 0; k < window.Length - 1; k++)
                        {
                            if (window[k] == 0 && window[k + 1] == 1)
                            {
                                jumpCount++;
                            }
                        }
                        var a = window[0]*window[2]*window[4];
                        var b = window[2] * window[4] * window[6];
                        if (sum >= 2 && sum <= 6 && jumpCount == 1 && a == 0 && b == 0)
                        {
                            rgbArray[i*bitmapRd.Width + j] = 255;
                            isChange = true;
                        }
                        else
                        {
                            thinRgbArray[i * bitmapRd.Width + j] = 0;
                        }
                    }
                }

                for (var i = 1; i < bitmapRd.Height - 1; i++)
                {
                    for (var j = 1; j < bitmapRd.Width - 1; j++)
                    {
                        if (rgbArray[i * bitmapRd.Width + j] == 255)
                        {
                            thinRgbArray[i * bitmapRd.Width + j] = 255;
                            continue;
                        }
                        var window = GetWindow(rgbArray, i, j, bitmapRd.Width);
                        var sum = window.Sum() - window[0];
                        var jumpCount = 0;
                        for (var k = 0; k < window.Length - 1; k++)
                        {
                            if (window[k] == 0 && window[k + 1] == 1)
                            {
                                jumpCount++;
                            }
                        }
                        var a = window[0] * window[2] * window[6];
                        var b = window[0] * window[4] * window[6];
                        if (sum >= 2 && sum <= 6 && jumpCount == 1 && a == 0 && b == 0)
                        {
                            rgbArray[i * bitmapRd.Width + j] = 255;
                            isChange = true;
                        }
                        else
                        {
                            thinRgbArray[i * bitmapRd.Width + j] = 0;
                        }
                    }
                }
                rgbArray = thinRgbArray;
            }
            var l = 0;
            for (var i = 0; i < rgbArray.Length; i++)
            {
                bitmapRd.RgbArray[l++] = rgbArray[i];
                bitmapRd.RgbArray[l++] = rgbArray[i];
                bitmapRd.RgbArray[l++] = rgbArray[i];
            }
            var newBitmapRd = new BitmapRawData(bitmapRd.RgbArray, bitmapRd.AlphaArray, bitmapRd.Width, bitmapRd.Height);
            return BitmapImageConverter.ConvertToImageFromRawData(newBitmapRd);
        }

        private static int[] GetWindow(IReadOnlyList<int> rgbArray, int i, int j, int width)
        {
            var window = new int[9];
            window[0] = rgbArray[(i - 1) * width + j] / 255;
            window[1] = rgbArray[(i - 1) * width + (j + 1)] / 255;
            window[2] = rgbArray[i * width + (j + 1)] / 255;
            window[3] = rgbArray[(i + 1) * width + (j + 1)] / 255;
            window[4] = rgbArray[(i + 1) * width + j] / 255;
            window[5] = rgbArray[(i + 1) * width + (j - 1)] / 255;
            window[6] = rgbArray[i * width + (j - 1)] / 255;
            window[7] = rgbArray[(i - 1) * width + (j - 1)] / 255;
            window[8] = rgbArray[(i - 1) * width + j] / 255;
            return window;
        }
    }
}
