﻿using System;
using System.Drawing;
using ViensScanner.Domain.Contracts;

namespace ViensScanner.Domain.Algorithms.Binarization
{
    public class BradleyBinarizationAlgorithm : IBinarizationAlgorithm
    {
        private const double T = 0.05;

        private const int D = 16;

        private long[] _integralImg;

        public Bitmap Binarization(Bitmap image)
        {
            var bitmapRd = BitmapImageConverter.ConvertToRawDataFromImage(image);
            var rgbArray = new int[bitmapRd.Width*bitmapRd.Height];
            for (var i = 0; i < bitmapRd.RgbArray.Length; i++)
            {
                if (i%3 == 0)
                {
                    rgbArray[i/3] = bitmapRd.RgbArray[i];
                }
            }
            _integralImg = new long[rgbArray.Length];
            for (var i = 0; i < bitmapRd.Width; i++)
            {
                var sum = 0;
                for (var j = 0; j < bitmapRd.Height; j++)
                {
                    sum += rgbArray[j*bitmapRd.Width + i];
                    if (i == 0)
                        _integralImg[j*bitmapRd.Width + i] = sum;
                    else _integralImg[j*bitmapRd.Width + i] = _integralImg[j*bitmapRd.Width + i - 1] + sum;
                }
            }
            for (var i = 0; i < bitmapRd.Width; i++)
            {
                for (var j = 0; j < bitmapRd.Height; j++)
                {
                    var xStart = i - bitmapRd.Width / D;
                    var xEnd = i + bitmapRd.Width / D;
                    var yStart = j - bitmapRd.Height / D;
                    var yEnd = j + bitmapRd.Height / D;
                    if (xStart < 0) xStart = 0;
                    if (xEnd >= bitmapRd.Width) xEnd = bitmapRd.Width - 1;
                    if (yStart < 0) yStart = 0;
                    if (yEnd >= bitmapRd.Height) yEnd = bitmapRd.Height - 1;
                    var count = (xEnd - xStart)*(yEnd - yStart);
                    var sum = _integralImg[yEnd*bitmapRd.Width + xEnd] -
                              _integralImg[yStart*bitmapRd.Width + xEnd] -
                              _integralImg[yEnd*bitmapRd.Width + xStart] +
                              _integralImg[yStart*bitmapRd.Width + xStart];
                    if (rgbArray[j*bitmapRd.Width + i]*count < (1.0 - T)*sum)
                    {
                        rgbArray[j*bitmapRd.Width + i] = 255;
                    }
                    else
                    {
                        rgbArray[j * bitmapRd.Width + i] = 0;
                    }
                }
            }
            var l = 0;
            for (var i = 0; i < rgbArray.Length; i++)
            {
                bitmapRd.RgbArray[l++] = rgbArray[i];
                bitmapRd.RgbArray[l++] = rgbArray[i];
                bitmapRd.RgbArray[l++] = rgbArray[i];
            }
            var newBitmapRd = new BitmapRawData(bitmapRd.RgbArray, bitmapRd.AlphaArray, bitmapRd.Width, bitmapRd.Height);
            return BitmapImageConverter.ConvertToImageFromRawData(newBitmapRd);
        }

        public Bitmap LocalBinarization(Bitmap image, Bitmap mask)
        {
            throw new NotImplementedException();
        }
    }
}
