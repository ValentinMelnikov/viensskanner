﻿using System;
using System.Drawing;
using ViensScanner.Domain.Contracts;

namespace ViensScanner.Domain.Algorithms.Binarization
{
    public class OtsuBinarizationAlgorithm: IBinarizationAlgorithm
    {
        public Bitmap Binarization(Bitmap image)
        {
            var bitmapRd = BitmapImageConverter.ConvertToRawDataFromImage(image);
            var rgbArray = new int[bitmapRd.RgbArray.Length];
            Array.Copy(bitmapRd.RgbArray, rgbArray, bitmapRd.RgbArray.Length);
            var min = rgbArray[0];
            var max = rgbArray[0];
            for (var i = 1; i < rgbArray.Length; i++)
            {
                var value = rgbArray[i];
                if (value < min) min = value;
                if (value > max) max = value;
            }
            var histSize = max - min + 1;
            var hist = new int[histSize];
            for (var t = 0; t < histSize; t++) hist[t] = 0;
            foreach (var t in rgbArray)
                hist[t - min]++;
            var m = 0;
            var n = 0;
            for (var t = 0; t <= max - min; t++)
            {
                m += t * hist[t];
                n += hist[t];
            }
            float maxSigma = -1;
            var threshold = 0;
            var alpha1 = 0;
            var beta1 = 0;
            for (var t = 0; t < max - min; t++)
            {
                alpha1 += t * hist[t];
                beta1 += hist[t];
                var w1 = (float)beta1 / n;
                var a = (float)alpha1 / beta1 - (float)(m - alpha1) / (n - beta1);
                var sigma = w1 * (1 - w1) * a * a;
                if (sigma > maxSigma)
                {
                    maxSigma = sigma;
                    threshold = t;
                }
            }
            threshold += min;
            for (var i = 0; i < rgbArray.Length; i++)
            {
                if (rgbArray[i] > threshold) rgbArray[i] = 255;
                else rgbArray[i] = 0;
            }
            var newBitmapRd = new BitmapRawData(rgbArray, bitmapRd.AlphaArray, bitmapRd.Width, bitmapRd.Height);
            return BitmapImageConverter.ConvertToImageFromRawData(newBitmapRd);
        }

        public Bitmap LocalBinarization(Bitmap image, Bitmap mask)
        {
            var bitmapRd = BitmapImageConverter.ConvertToRawDataFromImage(image);
            var rgbArray = new int[bitmapRd.RgbArray.Length];
            Array.Copy(bitmapRd.RgbArray, rgbArray, bitmapRd.RgbArray.Length);
            var maskBitmapRd = BitmapImageConverter.ConvertToRawDataFromImage(mask);
            var maskRgbArray = new int[maskBitmapRd.RgbArray.Length];
            Array.Copy(maskBitmapRd.RgbArray, maskRgbArray, maskBitmapRd.RgbArray.Length);
            var min = rgbArray[0];
            var max = rgbArray[0];
            for (var i = 1; i < rgbArray.Length; i++)
            {
                if (maskRgbArray[i] != 255) continue;
                var value = rgbArray[i];
                if (value < min) min = value;
                if (value > max) max = value;
            }
            var histSize = max - min + 1;
            var hist = new int[histSize];
            for (var t = 0; t < histSize; t++) hist[t] = 0;
            for (var t = 0; t < rgbArray.Length; t++)
            {
                if (maskRgbArray[t] != 255) continue;
                hist[rgbArray[t] - min]++;
            }
            var m = 0;
            var n = 0;
            for (var t = 0; t <= max - min; t++)
            {
                m += t * hist[t];
                n += hist[t];
            }
            float maxSigma = -1;
            var threshold = 0;
            var alpha1 = 0;
            var beta1 = 0;
            for (var t = 0; t < max - min; t++)
            {
                alpha1 += t * hist[t];
                beta1 += hist[t];
                var w1 = (float)beta1 / n;
                var a = (float)alpha1 / beta1 - (float)(m - alpha1) / (n - beta1);
                var sigma = w1 * (1 - w1) * a * a;
                if (sigma > maxSigma)
                {
                    maxSigma = sigma;
                    threshold = t;
                }
            }
            threshold += min;
            for (var i = 0; i < rgbArray.Length; i++)
            {
                if (maskRgbArray[i] == 255)
                {
                    if (rgbArray[i] > threshold) rgbArray[i] = 255;
                    else rgbArray[i] = 0;
                }
                else rgbArray[i] = 0;
            }
            var newBitmapRd = new BitmapRawData(rgbArray, bitmapRd.AlphaArray, bitmapRd.Width, bitmapRd.Height);
            return BitmapImageConverter.ConvertToImageFromRawData(newBitmapRd);
        }
    }
}
