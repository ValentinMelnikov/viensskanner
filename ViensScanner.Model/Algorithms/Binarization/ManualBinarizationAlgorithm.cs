﻿using System;
using System.Drawing;
using ViensScanner.Domain.Contracts;

namespace ViensScanner.Domain.Algorithms.Binarization
{
    public class ManualBinarizationAlgorithm : IBinarizationAlgorithm
    {

        private const byte Treshold = 25;

        public Bitmap Binarization(Bitmap image)
        {
            var bitmapRd = BitmapImageConverter.ConvertToRawDataFromImage(image);
            var rgbArray = new int[bitmapRd.Width * bitmapRd.Height];
            for (var i = 0; i < bitmapRd.RgbArray.Length; i++)
            {
                if (i % 3 == 0)
                {
                    rgbArray[i / 3] = bitmapRd.RgbArray[i];
                }
            }
            for (var i = 0; i < bitmapRd.Width; i++)
            {
                for (var j = 0; j < bitmapRd.Height; j++)
                {
                    if (rgbArray[j * bitmapRd.Width + i] < Treshold)
                    {
                        rgbArray[j * bitmapRd.Width + i] = 255;
                    }
                    else
                    {
                        rgbArray[j * bitmapRd.Width + i] = 0;
                    }
                }
            }
            var l = 0;
            for (var i = 0; i < rgbArray.Length; i++)
            {
                bitmapRd.RgbArray[l++] = rgbArray[i];
                bitmapRd.RgbArray[l++] = rgbArray[i];
                bitmapRd.RgbArray[l++] = rgbArray[i];
            }
            var newBitmapRd = new BitmapRawData(bitmapRd.RgbArray, bitmapRd.AlphaArray, bitmapRd.Width, bitmapRd.Height);
            return BitmapImageConverter.ConvertToImageFromRawData(newBitmapRd);
        }

        public Bitmap LocalBinarization(Bitmap image, Bitmap mask)
        {
            throw new NotImplementedException();
        }
    }
}
