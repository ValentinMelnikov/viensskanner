﻿using System.Drawing;
using ViensScanner.Domain.Contracts;

namespace ViensScanner.Domain.Algorithms
{
    public class AverageEqualizationAlgorithm : IEqualizationAlgorithm
    {
        public Bitmap Equalization(Bitmap image)
        {
            var bitmapRd = BitmapImageConverter.ConvertToRawDataFromImage(image);
            var rgbArray = new int[bitmapRd.Width * bitmapRd.Height];
            for (var i = 0; i < bitmapRd.RgbArray.Length; i++)
            {
                if (i % 3 == 0)
                {
                    rgbArray[i / 3] = bitmapRd.RgbArray[i];
                }
            }
            var brightDistrib = new int[256];
            for (var i = 0; i < image.Width; i++)
            {
                for (var j = 0; j < image.Height; j++)
                {
                    brightDistrib[rgbArray[j * bitmapRd.Width + i]]++;
                }
            }
            var n = new byte[256];
            var left = new byte[256];
            var right = new byte[256];
            var z = 0;
            var hint = 0;
            var havg = rgbArray.Length / brightDistrib.Length;
            for (var j = 0; j < brightDistrib.Length; j++)
            {
                if (z > 255) left[j] = 255;
                else left[j] = (byte)z;
                hint += brightDistrib[j];
                while (hint > havg)
                {
                    hint -= havg;
                    z++;
                }
                if (z > 255) right[j] = 255;
                else right[j] = (byte)z;
                n[j] = (byte)((left[j] + right[j]) / 2);
            }
            for (var i = 0; i < rgbArray.Length; i++)
            {
                if (left[rgbArray[i]] == right[rgbArray[i]]) rgbArray[i] = left[rgbArray[i]];
                else rgbArray[i] = n[rgbArray[i]];
            }
            var l = 0;
            for (var i = 0; i < rgbArray.Length; i++)
            {
                bitmapRd.RgbArray[l++] = rgbArray[i];
                bitmapRd.RgbArray[l++] = rgbArray[i];
                bitmapRd.RgbArray[l++] = rgbArray[i];
            }
            var newBitmapRd = new BitmapRawData(bitmapRd.RgbArray, bitmapRd.AlphaArray, bitmapRd.Width, bitmapRd.Height);
            return BitmapImageConverter.ConvertToImageFromRawData(newBitmapRd);
        }
    }
}
