﻿using System.Drawing;
using System.IO;

namespace ViensScanner.Domain
{
    public class BitmapImageConverter
    {
        public static BitmapRawData ConvertToRawDataFromImage(Bitmap image)
        {
            var rgbArray = new int[3 * image.Width * image.Height];
            var alphaArray = new int[image.Width * image.Height];
            var rgbIndex = 0;
            var alphaIndex = 0;

            for (var x = 0; x < image.Height; x++)
            {
                for (var y = 0; y < image.Width; y++)
                {
                    var pixelColor = image.GetPixel(y, x);
                    rgbArray[rgbIndex++] = pixelColor.R;
                    rgbArray[rgbIndex++] = pixelColor.G;
                    rgbArray[rgbIndex++] = pixelColor.B;
                    alphaArray[alphaIndex++] = pixelColor.A;
                }
            }

            return new BitmapRawData(rgbArray, alphaArray, image.Width, image.Height);
        }

        public static Bitmap ConvertToImageFromRawData(BitmapRawData imageRawData)
        {
            var rgbArray = imageRawData.RgbArray;

            var image = new Bitmap(imageRawData.Width, imageRawData.Height);
            var rgbIndex = 0;
            for (var x = 0; x < imageRawData.Height; x++)
            {
                for (var y = 0; y < imageRawData.Width; y++)
                {
                    var r = rgbArray[rgbIndex++];
                    var g = rgbArray[rgbIndex++];
                    var b = rgbArray[rgbIndex++];
                    image.SetPixel(y, x, Color.FromArgb(r, g, b));
                }
            }
            return image;
        }
    }
}
