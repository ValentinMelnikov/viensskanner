﻿namespace ViensScanner.Domain
{
    public enum SingularPoints
    {
        End = 1,
        None,
        Bifurcation,
        Crossing
    }
}
