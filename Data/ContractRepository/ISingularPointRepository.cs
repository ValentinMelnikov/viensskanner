﻿using System.Collections.Generic;

namespace Data.ContractRepository
{
    public interface ISingularPointRepository
    {
        ICollection<SingularPoint> GetAll();
        void Save(SingularPoint point);
    }
}