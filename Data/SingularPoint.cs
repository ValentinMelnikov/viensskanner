﻿using ViensScanner.Domain;

namespace Data
{
    public class SingularPoint
    {
        public int X { get; set; }

        public int Y { get; set; }

        public SingularPoints Type { get; set; }
    }
}
