﻿using System.Drawing;

namespace ViensScanner.Contract
{
    public interface IImageService
    {
        Bitmap GrayShadin(Bitmap image);

        Bitmap Contrastin(Bitmap image);

        Bitmap Filterin(Bitmap image);

        Bitmap Equalization(Bitmap image);

        Bitmap Binarization(Bitmap image);

        Bitmap LocalBinarization(Bitmap image, Bitmap mask);

        Bitmap Thinning(Bitmap image);

        Bitmap FindSingularPoints(Bitmap image);

        Bitmap EdgeDetection(Bitmap image);
    }
}
