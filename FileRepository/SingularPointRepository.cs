﻿using System;
using System.Collections.Generic;
using System.IO;
using Data;
using Data.ContractRepository;

namespace FileRepository
{
    public class SingularPointRepository : ISingularPointRepository
    {
        private const string _fileName = "Veins.dat";

        public ICollection<SingularPoint> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public void Save(SingularPoint point)
        {
            using (var fs = new FileStream(_fileName, FileMode.Append, FileAccess.Write))
            {
                using (var sw = new StreamWriter(fs))
                {
                    sw.WriteLine($"{point.X} {point.Y} {point.Type}{Environment.NewLine}");
                }
            }
        }
    }
}
